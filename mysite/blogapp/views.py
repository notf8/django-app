from django.contrib.syndication.views import Feed
from django.urls import reverse_lazy, reverse
from .models import Article
from django.views.generic import ListView, CreateView, DetailView


class ArticlesListView(ListView):
    template_name = "blogapp/articles-list.html"
    context_object_name = "articles"
    queryset = (
        Article.objects
        .select_related("author", "category")
        .prefetch_related("tags")
        .filter(pub_date__isnull=False)
        .order_by("-pub_date")
    )


class ArticleDetailView(DetailView):
    queryset = (
        Article.objects
        .select_related("author")
        .prefetch_related("tags")
    )


class CreateArticleView(CreateView):
    model = Article
    fields = "title", "content", "author", "category", "tags",
    success_url = reverse_lazy("blogapp:articles_list")

    def form_valid(self, form):
        form.instance.author_id = [self.request.user.id, "test_author_2"]
        return super().form_valid(form)


class LatestArticlesFeed(Feed):
    title = "Blog articles (latest)"
    description = "Updates on changes and addition blog articles"
    link = reverse_lazy("blogapp:articles_list")

    def items(self):
        return (
            Article.objects
            .select_related("author", "category")
            .prefetch_related("tags")
            .filter(pub_date__isnull=False)
            .order_by("-pub_date")[:5]
        )

    def item_title(self, item: Article):
        return item.title

    def item_description(self, item: Article):
        return item.content[:200]


