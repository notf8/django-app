from django.urls import path
from .views import (
    ArticlesListView,
    CreateArticleView,
    ArticleDetailView,
    LatestArticlesFeed,
)

app_name = "blogapp"

urlpatterns = [
    path("articles/", ArticlesListView.as_view(), name="articles_list"),
    path("articles/<int:pk>/", ArticleDetailView.as_view(), name="article_details"),
    path("articles/create/", CreateArticleView.as_view(), name="create_article"),
    path("articles/latest/feed/", LatestArticlesFeed(), name="articles-feed"),
]

