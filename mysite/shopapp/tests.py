from string import ascii_letters
from random import choices
from django.conf import settings
from django.contrib.auth.models import Permission
from django.test import TestCase
from django.urls import reverse

from shopapp.models import Product, User, Order
from shopapp.utils import add_two_numbers


class AddTwoNumbersTestCase(TestCase):
    def test_add_two_numbers(self):
        result = add_two_numbers(2, 3)
        self.assertEqual(result, 5)


class ProductCreateTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.credentials = dict(username="bob", password="qwerty")
        cls.user = User.objects.create_user(**cls.credentials)
        permission_order = Permission.objects.get(codename='add_product')
        cls.user.user_permissions.add(permission_order)
        cls.user.save()

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def test_product_create_view_no_permission(self):
        self.client.logout()
        response = self.client.get(reverse("shopapp:product_create"))
        self.assertRedirects(response, str(settings.LOGIN_URL))
        # self.assertEqual(response.status_code, 302)
        # self.assertIn(str(settings.LOGIN_URL), response.url)

    def setUp(self) -> None:
        self.client.login(**self.credentials)
        self.product_name = "".join(choices(ascii_letters, k=10))
        Product.objects.filter(name=self.product_name).delete()

    def test_create_product(self):
        response = self.client.post(
            reverse("shopapp:product_create"),
            {
                "name": self.product_name,
                "price": "123.45",
                "description": "A good table",
                "discount": "10",
                "created_by": "Admin",
            }, HTTP_USER_AGENT='Mozilla/5.0'
        )
        self.assertRedirects(response, reverse("shopapp:product_create"))
        self.assertTrue(
            Product.objects.filter(name=self.product_name).exist()
        )


class ProductDetailsViewTestCase(TestCase):
    # @classmethod
    # def setUpClass(cls):
    #     cls.product = Product.objects.create(name="Best Product", created_by_id="1")

    def setUp(self) -> None:
        self.product = Product.objects.create(name="Best Product", created_by_id="1")

    # @classmethod
    # def tearDownClass(cls):
    #     cls.product.delete()

    def tearDown(self) -> None:
        self.product.delete()

    def test_get_product(self):
        response = self.client.get(
            reverse("shopapp:product_details", kwargs={"pk": self.product.pk})
        )
        self.assertEqual(response.status_code, 200)

    def test_get_product_and_check_content(self):
        response = self.client.get(
            reverse("shopapp:product_details", kwargs={"pk": self.product.pk})
        )
        self.assertContains(response, self.product.name)


class ProductsListViewTestCase(TestCase):
    fixtures = [
        'products-fixture.json',
        'users-fixture.json',
    ]

    def test_products(self):
        response = self.client.get(reverse("shopapp:products_list"))
        self.assertQuerysetEqual(
            qs=Product.objects.filter(archived=False).all(),
            values=(p.pk for p in response.context["products"]),
            transform=lambda p: p.pk
        )
        self.assertTemplateUsed(response, 'shopapp/products-list.html')


class OrdersListViewTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.credentials = dict(username="bob_test", password="qwerty")
        cls.user = User.objects.create_user(**cls.credentials)

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.login(**self.credentials)

    def test_orders_view(self):
        response = self.client.get(reverse("shopapp:orders_list"))
        self.assertContains(response, "Orders")

    def test_orders_view_not_authenticated(self):
        self.client.logout()
        response = self.client.get(reverse("shopapp:orders_list"))
        # self.assertRedirects(response, str(settings.LOGIN_URL))
        self.assertEqual(response.status_code, 302)
        self.assertIn(str(settings.LOGIN_URL), response.url)


class OrderExportTestCase(TestCase):
    fixtures = [
        "products-fixture.json",
        "users-fixture.json",
        "orders-fixture.json",
    ]

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user(username="Test_user", password="qwerty", is_staff=True)

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.force_login(self.user)

    def test_get_orders_view(self):
        response = self.client.get(
            reverse("shopapp:orders-export"),
        )
        self.assertEqual(response.status_code, 200)
        orders = Order.objects.order_by("pk").all()
        expected_data = [
            {
                "pk": order.pk,
                "address": order.delivery_address,
                "promocode": order.promocode,
                "user": order.user,
                "products": order.products
            }
            for order in orders
        ]
        orders_data = response.json()
        self.assertEqual(
            orders_data["orders"],
            expected_data,
        )


class OrderDetailViewTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user(username="Test_user", password="qwerty")
        permission_order = Permission.objects.get(codename='view_order')
        cls.user.user_permissions.add(permission_order)
        cls.user.save()

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.force_login(self.user)
        self.order = Order.objects.create(
                delivery_address="Test address",
                promocode="sale_1",
                user_id=self.user.pk,
        )

    def tearDown(self) -> None:
        self.order.delete()

    def test_order_details(self):
        response = self.client.get(
            reverse("shopapp:order_details", kwargs={"pk": self.order.pk}),
        )
        received_data = response.context["order"].pk
        expected_data = self.order.pk
        self.assertContains(response, self.order.delivery_address)
        self.assertContains(response, self.order.promocode)
        self.assertEqual(received_data, expected_data)
