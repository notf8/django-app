from django.contrib.auth.models import User
from django.core.management import BaseCommand
from shopapp.models import Product


class Command(BaseCommand):

    def handle(self, *args, **options):
        self.stdout.write("Start demo bulk_actions")

        result = Product.objects.filter(
            name__contains="smartphone",
        ).update(discount=10)
        print(result)
        # info = [
        #     ("smartphone1", 1999),
        #     ("smartphone2", 2999),
        #     ("smartphone3", 3999),
        #     ("smartphone4", 4999),
        # ]
        # products = [
        #     Product(name=name, price=price, created_by_id=1)
        #     for name, price in info
        # ]
        # result = Product.objects.bulk_create(products)
        # for obj in result:
        #     print(obj)
        self.stdout.write("Done")
