from django.http import HttpRequest, HttpResponse
from datetime import datetime, timedelta


def set_useragent_on_request_middleware(get_response):

    print("Initial call")

    def middleware(request: HttpRequest):
        request.user_agent = request.META["HTTP_USER_AGENT"]
        response = get_response(request)
        return response

    return middleware


class RequestThrottling:
    def __init__(self, get_response):
        self.get_response = get_response
        self.count = 0
        self.current_time = datetime.now()
        self.time_to_stop = datetime.now() + timedelta(seconds=+60)
        self.seconds = 60

    def __call__(self, request: HttpRequest):
        current_ip = request.META.get('REMOTE_ADDR')
        check_user = {current_ip: self.count}  # TODO эта переменная существует только в время конкретного запроса
        # пользователя и после этого она изчезает, храните словарь в атрибуте класса, или в глобальной переменной или
        # в файлае
        while self.time_to_stop > self.current_time:  # TODO цикл тут не нужен
            self.current_time = datetime.now()
            if check_user[current_ip] <= 5:
                response = self.get_response(request)
                self.count += 1
                print("Проверка количества запросов:", self.count)
                return response
            else:
                remain_time_to_repeat = self.time_to_stop - self.current_time
                if 60 > remain_time_to_repeat.seconds:
                    return HttpResponse(f"Количество запросов превышено, повторите попытку через "
                                        f"{remain_time_to_repeat.seconds} секунд(ы)")
        else:
            self.current_time = datetime.now()
            self.time_to_stop = datetime.now() + timedelta(seconds=+60)
            response = self.get_response(request)
            self.count = 0
            return response
# TODO  Попробуйте сделать так:
#  - храним данные по посещениях в словаре
#  - при запросе смотрим в словарь по ключу с ip, если его нет, создаём запись вида "ip: време доступа", и всё, а если
#  ключ есть, то получаем время прошлого доступа
#  - сравниваем текущее время и время последнего запроса, если разница меньше допустимого - возвращаем страницу с
#  ошибкой. Если разница допустима - обновляем время доступа для этого ip.
