from django.core.files.storage import FileSystemStorage
from django.http import HttpRequest, HttpResponse
from django.shortcuts import render


def handle_file_upload(request: HttpRequest) -> HttpResponse:

    link = '<h3><a href="http://127.0.0.1:8000/req/upload/">Выбрать другой файл</a></h3>'

    if request.method == "POST" and request.FILES.get("myfile"):
        myfile = request.FILES["myfile"]
        fs = FileSystemStorage()
        if myfile.size <= 1048576:
            filename = fs.save(myfile.name, myfile)
            print("Saved file: ", filename)
        else:
            return HttpResponse(f"<h1>Размер файла превышает 1 мб {link}</h1>",)
    return render(request, "requestdataapp/file-upload.html",)
